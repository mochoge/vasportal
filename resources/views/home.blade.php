@extends('layouts.app')

@section('content')
    <div class="row tile_count">
        @livewire('home.statistics')
    </div>

    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Recent Activities</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @livewire('projects.activities')
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Active Projects</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @livewire('projects.active')
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Recent posts</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="dashboard-widget-content">
                        @livewire('projects.posts')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="{!! asset('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}"></script>
@stop