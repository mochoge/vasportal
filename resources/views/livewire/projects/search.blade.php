<div class="x_panel">
<div class="x_title">
    <div class="row">
        <div class="col-md-6">
        <div class="input-group">
            <input wire:model="search" type="text" class="form-control" name="search"
                placeholder="Search Projects"> <span class="input-group-btn">
                <button wire:click="clear" class="btn btn-default">
                    Clear
                </button>
            </span>
        </div>
        </div>
        <div class="col-md-6">
            <ul class="nav navbar-right panel_toolbox">
                </li>
                @can('Create projects')
                    <li><a data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-plus"></i></a>
                    </li>
                @endcan
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="table-responsive">
    @if($projects->total() > 0)
        <div class="row m-4">
            <div class="col form-inline">
                &nbsp &nbsp Per Page: &nbsp
                <select wire:model="perPage" class="form-controll">
                    <option>5</option>
                    <option>10</option>
                    <option>30</option>
                    <option>50</option>
                    <option>80</option>
                    <option>100</option>
                </select>
            </div>
        </div>

        <table class="table table-striped table-sm  no-margin">
            <thead>
            <tr>
                <th><a wire:click.prevent="sortBy('id')" role="button" href="#">Id <i class="fa fa-sort"></i> </a></th>
                <th><a wire:click.prevent="sortBy('name')" role="button" href="#">Name <i class="fa fa-sort"></i> </a></th>
                <th><a wire:click.prevent="sortBy('id')" role="button" href="#">Members <i class="fa fa-sort"></i> </a></th>
                <th><a wire:click.prevent="sortBy('completion_stage')" role="button" href="#">Project progress <i class="fa fa-sort"></i> </a></th>
                <th><a wire:click.prevent="sortBy('status_badge')" role="button" href="#">Status <i class="fa fa-sort"></i> </a></th>
                <th><a wire:click.prevent="sortBy('description')" role="button" href="#">Description <i class="fa fa-sort"></i> </a></th>
                <th><a wire:click.prevent="sortBy('tribe_id')" role="button" href="#">Tribe <i class="fa fa-sort"></i> </a></th>
                <th><a wire:click.prevent="sortBy('squad_id')" role="button" href="#">Squad <i class="fa fa-sort"></i> </a></th>
            </thead>
            <tbody>
            @foreach($projects as $project)
                <tr>
                    <td>{!! $project->id !!}</td>
                    <td>{!! $project->link !!}</td>
                    <td>{!! $project->members->count() !!}</td>
                    <td>
                        <div class="progress progress_sm">
                            <div class="progress-bar bg-green" role="progressbar"
                                    data-transitiongoal="{!! $project->completion_level !!}"
                                    aria-valuenow="49"
                                    style="width: {!! $project->completion_level !!}%;"></div>
                        </div>
                    </td>
                    <td>{!! $project->status_badge !!}</td>
                    <td>{!! $project->description !!}</td>
                    <td>{!! $project->tribe_id !!}</td>
                    <td>{!! $project->squad_id !!}</td>
                    <td>
                        @can('Delete projects')
                            <button id="{!! $project->id !!}"
                                    class="btn btn-danger btn-xs delete-client"><i
                                        class="fa  {!! $project->trashed()? 'fa-recycle':'fa-trash' !!}"></i>
                            </button>
                        @endcan

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="row">
        @if($projects->first())
            <!-- Add Appends(search term) -->
            <div class="col">
                &nbsp &nbsp {{ $projects->links() }}
            </div>
            <div class="col text-right text-muted">
                showing {{ $projects->firstItem() }} to {{ $projects->lastItem() }} out of {{ $projects->total() }} projects &nbsp &nbsp
            </div>
        @endif
        </div>
    @endif

</div>