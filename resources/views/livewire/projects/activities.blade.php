<div class="dashboard-widget-content">
    <ul class="list-unstyled timeline widget">
        @foreach($updates as $update)
            <li>
                <div class="block">
                    <div class="block_content">
                        <h2 class="title">
                            <a>{!! $update->project->name !!}</a>
                        </h2>
                        <div class="byline">
                            <span>{!! $update->created_at->diffForHumans() !!}</span> by
                            <a>{!! $update->user->name !!}</a>
                        </div>
                        <p class="excerpt">{!! $update->description !!}</p>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
    <div class="row">
    @if($updates->first())
        <div class="col">
            &nbsp &nbsp {{ $updates->links() }}
        </div>
        <div class="col text-right text-muted">
            showing {{ $updates->firstItem() }} to {{ $updates->lastItem() }} out of {{ $updates->total() }} Activites &nbsp &nbsp
        </div>
    @endif
    </div>
</div>