@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Tribes</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                        @can('Create tribes')
                            <li><a data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                            class="fa fa-user-plus"></i></a>
                            </li>
                        @endcan
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th>Tribe Id</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tribes as $tribe)
                                <tr>
                                    <td>{!! $tribe->id !!}</td>
                                    <td>{!! $tribe->link !!}</td>
                                    <td>{!! $tribe->description !!}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $tribes->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    @can('Create tribes')
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Create Tribe</h4>
                    </div>
                    {{ Form::open(['route' => 'tribes.store']) }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Tribe Name<span class="required">*</span></label>
                            {!!  Form::text('name', $value = null, $attributes =[
                            'class' => 'form-control'
                            ]); !!}
                        </div>
                        <div class="form-group">
                            <label>Tribe Description<span class="required">*</span></label>
                            {!!  Form::text('description', $value = null, $attributes =[
                            'class' => 'form-control'
                            ]); !!}
                        </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Create Tribe</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    @endcan
@stop
