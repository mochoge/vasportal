@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
                                   aria-expanded="true">Integrated projects</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content3" role="tab" id="profile-tab2"
                                   data-toggle="tab" aria-expanded="false">Profile</a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                 aria-labelledby="home-tab">
                                <table class="table table-striped table-sm no-margin">
                                    <thead>
                                    <tr>
                                        <th>User Id</th>
                                        <th>
                                            Name
                                        </th>
                                        <th>Email</th>
                                        <th>Projects</th>
                                        <th>Last connected</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($system->projects as $user)
                                        <tr>
                                            <td>{!! $user->id !!}</td>
                                            <td>{!! $user->link !!}</td>
                                            <td>{!! $user->email !!}</td>
                                            <td>0</td>
                                            <td>{!! $user->updated_at->diffForHumans() !!}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content3"
                                 aria-labelledby="profile-tab">
                                {{ Form::model($system,['route' => ['systems.update', $system->id],'method' => 'put']) }}
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Role name<span class="required">*</span></label>
                                        {!!  Form::text('name', $system->name, $attributes =[
                                        'class' => 'form-control'
                                        ]); !!}
                                    </div>
                                    <div class="form-group">
                                        <label>Role description<span class="required">*</span></label>
                                        <textarea class="form-control" name="description"
                                                  placeholder="Role description">{!! $system->description !!}</textarea>
                                    </div>
                                </div>

                                @can('Edit core systems')
                                    <button type="submit" class="btn btn-primary">Update</button>
                                @endcan

                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
