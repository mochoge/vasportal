<?php

namespace App\Http\Controllers;

use App\ProjectUpdate;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function updatesReport(Request $request)
    {
        $page_name = "Updates report";
        $start_date = Carbon::now()->startOfWeek();
        $end_date = Carbon::now()->endOfWeek();
        if (isset($request->dates)) {
            $datesArray = explode("-", $request->dates);
            if (count($datesArray) == 2) {
                $start_date = Carbon::parse($datesArray[0]);
                $end_date = Carbon::parse($datesArray[1]);
            }
        }

        $updates = ProjectUpdate::where(function ($query) use ($request) {
            if (isset($request->users))
                if (!in_array("*", $request->users))
                    $query->whereIn('user_id', $request->users);

            if (isset($request->projects))
                if (!in_array("*", $request->projects))
                    $query->whereIn('project_id', $request->projects);

            return $query;
        })->whereBetween('created_at', [$start_date, $end_date])
            ->get();
        return view('reports.updates', compact('page_name', 'updates'));
    }
}
