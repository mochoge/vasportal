<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectUpdate;
use App\ProjectUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ProjectController extends Controller
{
    public function myProjects()
    {
        return view('projects.index');
    }

    public function index()
    {
        return view('projects.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @TODO: Add squad name
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:projects,name',
            'description' => 'sometimes'
            // 'datetime' => 'required'
        ]);

        // split date
        $date = explode(' - ', $request->datetime);

        Project::updateOrCreate([
            'name' => $request->name,
            'start_date' => Carbon::now(),
            'description' => $request->description
            // 'start_date' => Carbon::createFromFormat('Y-m-d H:i:s', $date[0]),
            // 'end_date' => Carbon::createFromFormat('Y-m-d H:i:s', $date[1])
        ]);
        $request->session()->flash('success', 'Project created successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $page_name = $project->name . ' details';
        // get previous project id
        $previous = Project::where('id', '<', $project->id)->max('id');

        // get next project id
        $next = Project::where('id', '>', $project->id)->min('id');

        return view('projects.show', compact('project', 'page_name', 'previous', 'next'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Project $project)
    {
        $this->validate($request, [
            'name' => [
                'required',
                Rule::unique('projects')->ignore($project)
            ],
            'description' => 'sometimes',
        ]);

        Project::updateOrCreate(
            [
                'id' => $project->id
            ],
            [
                'name' => $request->name,
                'description' => $request->description
            ]
        );
        $request->session()->flash('success', 'Project updated successfully');
        return back();
    }

    /**
     * @param Request $request
     * @param $projectId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function saveUpdate(Request $request, $projectId)
    {
        // dd($request);

        $project = Project::findOrFail($projectId);
        $this->validate($request, [
            'title' => 'required',
            'completion_level' => 'required',
            'datetime' => 'required',
            'description' => 'required',
        ]);
        // split date
        $date = explode(' - ', $request->datetime);

        // dd(Carbon::createFromFormat("Y-m-d H:i", $date[0]));


        ProjectUpdate::updateOrCreate([
            'project_id' => $project->id,
            'user_id' => $request->user()->id,
            'project_user_id' => $request->user()->id,
            'update_subject' => 'None',
            'completion_level' => $request->completion_level,
            'title' => $request->title,
            'description' => $request->description,
            'start_date' => Carbon::createFromFormat("Y-m-d H:i", $date[0]),
            'end_date' => Carbon::createFromFormat("Y-m-d H:i", $date[1])
        ]);
        $request->session()->flash('success', 'Project update save');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, Project $project)
    {
        $project->delete();
        $request->session()->flash("success", "Project deleted");
        return route("projects.index");
    }


    public function joinProject(Request $request, $user_id, $project_id)
    {
        $project = Project::findOrFail($project_id);
        $user = User::findOrFail($user_id);
        ProjectUser::updateOrCreate(
            [
                'user_id' => $user_id,
                'project_id' => $project_id
            ],
            [
                'role' => 'Member',
                'role_description' => 'Test'
            ]
        );

        $request->session()->flash('succcess', strtoupper($user->link) . " joined project " . strtoupper($project->link));
        return back();
    }

    public function leaveProject(Request $request, $user_id, $project_id)
    {
        $project = Project::findOrFail($project_id);
        $user = User::findOrFail($user_id);
        ProjectUser::where(
            [
                'user_id' => $user_id,
                'project_id' => $project_id
            ]
        )->delete();

        $request->session()->flash('succcess', strtoupper($user->link) . " left project " . strtoupper($project->link));
        return back();
    }
}
