<?php

namespace App\Http\Livewire\Home;

use Livewire\Component;

class Statistics extends Component
{
    public function render()
    {
        return view('livewire.home.statistics', [
            'updates' => \App\ProjectUpdate::orderBy('id', 'DESC')->latest()->paginate(10),
            'posts' => \App\TeamBoard::orderBy('id', 'DESC')->paginate(10),
            'projects' => \App\Project::latest()->paginate(5)
        ]);
    }
}
