<?php

namespace App\Http\Livewire\Projects;

use Livewire\Component;
use Livewire\WithPagination;

class Posts extends Component
{
    use WithPagination;

    public function render()
    {
        return view('livewire.projects.posts', [
            'posts' => \App\TeamBoard::orderBy('id', 'DESC')->paginate(10)
        ]);
    }
}
