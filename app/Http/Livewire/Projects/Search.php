<?php

namespace App\Http\Livewire\Projects;

use Livewire\Component;

use Livewire\WithPagination;

class Search extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 10;
    public $sortField = 'id';
    public $sortAsc = true;

    public function sortBy($field)
    {
        if ($this->sortField == $field) {
            $this->sortAsc = ! $this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function clear()
    {
        $this->search = '';
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $search = '%'. $this->search .'%';
        $projects = '';

        if (request()->route()->getName() == 'projects.mine') {
            $projects = \Auth::user()->projects()
                    ->where('name', 'like', $search)
                    ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
                    ->paginate($this->perPage)
                    ->appends($search);
        } else {
            $projects = \App\Project::where('name', 'like', $search)
                    ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
                    ->paginate($this->perPage)
                    ->appends($search);
        }

        return view('livewire.projects.search', compact('projects'));
    }
}
