<?php

namespace App\Http\Livewire\Projects;

use Livewire\Component;
use Livewire\WithPagination;

class Activities extends Component
{
    use WithPagination;

    public function render()
    {
        return view('livewire.projects.activities', [
            'updates' => \App\ProjectUpdate::orderBy('id', 'DESC')->latest()->paginate(8)
        ]);
    }
}
