<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Squad extends Model
{
    public function tribes()
    {
        return $this->belongsTo(Tribe::class);
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
