<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::updateOrCreate(
            [
               'id' => 1
           ],
            ['name' => "Sys admin"
           ]
        );

        $role->givePermissionTo(Permission::all());
    }
}
