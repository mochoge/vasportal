<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $models = [
            'posts', 'projects', 'users', 'roles', 'core systems', 'tribes', 'squads'
        ];
        // create permissions
        foreach ($models as $model) {
            Permission::updateOrCreate(['name' => 'View ' . $model]);
            Permission::updateOrCreate(['name' => 'Create ' . $model]);
            Permission::updateOrCreate(['name' => 'Edit ' . $model]);
            Permission::updateOrCreate(['name' => 'Delete ' . $model]);
        }
        Permission::updateOrCreate(['name' => 'Join projects']);
        Permission::updateOrCreate(['name' => 'Reset user password']);
        Permission::updateOrCreate(['name' => 'Generate reports']);
        Permission::updateOrCreate(['name' => 'View project updates reports']);


        // create roles and assign created permissions
        $role = Role::updateOrCreate(['name' => 'Super admin']);
        $role->givePermissionTo(Permission::all());
    }
}
